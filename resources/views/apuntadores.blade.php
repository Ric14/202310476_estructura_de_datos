<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
</head>
<body>
    <h1>Práctica Apuntadores</h1>
    <p>Práctica de Apuntadores de variables en memoria.</p>
    <code>
        $edad = 26;<br>
        $Aedad = & $edad;<br>

        echo "{$edad}\n";<br>
        unset($edad);<br>
        echo "{$Aedad}\n";<br>
    </code>
    <h2>Resultado:</h2>
    <h4>{{$Aedad}}</h4>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <title>Interfaz</title>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Interfaz</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Fila1</a></li>
            <li><a href="#">Fila2</a></li>
            <li><a href="#">Fila3</a></li>
            <li><a href="#">Fila4</a></li>
            <li><a href="#">Fila5</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fila6<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<br>
    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Hola Mundo</h1>
      </div>
    </div>
    <div class="row">
        <div class="col-md-8">Columna1</div>
    </div>
<br>
    <div class="row">
        <div class="col-md-4">Columna2</div>
    </div>
<br>
    <div class="row">
        <div class="col-md-6">Columna3</div>
    </div>
<br>
    <div class="row">
        <div class="col-md-6">Columna4</div>
    </div>
<br>
    <div class="row">
        <div class="col-md-4">Columna5</div>
    </div>
<br>
    <div class="row">
        <div class="col-md-4">Columna6</div>
    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recursividad</title>
</head>
<body>
    <h1>Práctica Recursividad</h1>
    <p>Práctica de Recursividad.</p>
    <code>
    $N += 1;<br>
        if ($N<=$Nfin){<br>
            echo $N;<br>
            $this->Recursividad($Nfin, $N);<br>
        }<br>
            $this->Recursividad(10, 1); <br>
    </code>
    <h2>Resultado:</h2>
    <h4>{{$variable}}</h4>
</body>
</html>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memoria</title>
</head>
<body>
    <h1>Práctica Memoria</h1>
    <p>Práctica de gestión de memoria en php.</p>
    <code>
        echo 'Memoria en uso: ' . memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
        echo str_repeat("a",200000)."<br>";
        echo 'Memoria en uso: ' . memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
        echo 'Memoria limite: ' . ini_get('memory_limit') . '<br>';
    </code>
    <h2>Resultado:</h2>
    <h4>{{$Resultado}}</h4>
</body>
</html>
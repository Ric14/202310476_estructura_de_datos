<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecursividadController extends Controller
{
    public function Recursividad($Nfin, $N){
        $N += 1;
        if ($N<=$Nfin){
            echo $N;
            $this->Recursividad($Nfin, $N);
            return $N;
        }
        return $N;
    }
    public function Incrementable(){
      $variable = $this->Recursividad(25, 1);  
      return view('recursividad',['variable'=>$variable]);
    }
}
?>
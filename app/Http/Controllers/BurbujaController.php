<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BurbujaController extends Controller
{
    public function Burbuja($arreglo){
        $longitud = count($arreglo);
        for ($i = 0; $i < $longitud; $i++) {
            for ($j = 0; $j < $longitud - 1; $j++) {
                if ($arreglo[$j] > $arreglo[$j + 1]) {
                    $temporal = $arreglo[$j];
                    $arreglo[$j] = $arreglo[$j + 1];
                    $arreglo[$j + 1] = $temporal;
                }
            }
        }

        return $arreglo;
    }

    public function OrdenamientoBurbuja(){

        $miArreglo = [5, 1, 80, 10, 7, 66, 100];
        echo "Antes de ordenar: ";
        print_r($miArreglo);

        echo "<br>";
        $miArreglo = $this->Burbuja($miArreglo);
        echo "Después de ordenar: ";
        print_r($miArreglo);
    }
}
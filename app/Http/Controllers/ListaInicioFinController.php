<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;

class ListaInicioFinController extends Controller
{
    public function listainiciofin(){
        $lista = new SplDoublyLinkedList;

        $lista->push(1); 
        $lista->push(2); 
        $lista->push(3); 
        $lista->push(4); 
        $lista->push(5); 
        
        echo $lista->top()."<br>";

        echo $lista->bottom()."<br>";
    }
}

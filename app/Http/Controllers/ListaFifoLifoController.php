<?php

namespace App\Http\Controllers;

use SplDoublyLinkedList;

use Illuminate\Http\Request;

class ListaFifoLifoController extends Controller
{
    public function listaFifoLifo(){
        $lista = new SplDoublyLinkedList;
        $lista->setIteratormode(SplDoublyLinkedList::IT_MODE_LIFO);
        
        $lista->push(1);
        $lista->push(2);
        $lista->push(3);
        $lista->push(4);
        $lista->push(5);

        //print_r($lista);
        echo "<h2>Lista LIFO (pila)</h2>";
        $lista-> rewind();
        while ($lista->valid()){
            echo $lista->current()."<br>";
            $lista->next();
        }
        echo "<h2>Lista FIFO (cola)</h2>";
        $lista->setIteratormode(SplDoublyLinkedList::IT_MODE_FIFO);
        $lista-> rewind();
        while ($lista->valid()){
            echo $lista->current()."<br>";
            $lista->next();
        }

    }
}

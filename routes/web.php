<?php

use Illuminate\Support\Facades\Route; 
use App\Http\Controllers\InicioController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\InterfazGraficaController;
use App\Http\Controllers\AgregarListaIndiceController;
use App\Http\Controllers\ListaInicioFinController;
use App\Http\Controllers\ListaFifoLifoController;
use App\Http\Controllers\LoginArrayController;
use App\Http\Controllers\ArreglosController;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\BurbujaController;
use App\Http\Controllers\QuicksortController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apuntadores', [InicioController::class, 'Inicio']);
Route::get('/memoria', [MemoriaController::class, 'Memoria']);
Route::get('/recursividad', [RecursividadController::class, 'Incrementable']);
Route::get('/interfaz', [InterfazGraficaController::class, 'Inicio']);
Route::get('/agregar-lista-indice', [AgregarListaIndiceController::class, 'AgregarLista']);
Route::get('/lista-inicio-fin', [ListaInicioFinController::class, 'listainiciofin']);
Route::get('/lista-fifo-lifo', [ListaFifoLifoController::class, 'listaFifoLifo']);
Route::get('/login', [LoginArrayController::class, 'InicioLogin']);
Route::get('/login_validacion', [LoginArrayController::class, 'LoginValidacion']);
Route::get('/practica-arreglos', [ArreglosController::class, 'Arreglos']);
Route::get('/inicio-panel', [PanelController::class, 'InicioPanel']);
Route::get('/ordenamiento_burbuja', [BurbujaController::class, 'OrdenamientoBurbuja']);
Route::get('/ordenamiento_quicksort', [QuicksortController::class, 'OrdenamientoQuickSort']);